function validateFormBand(req, res, next) {
    const {nombre, integrantes, fecha_inicio, pais} = req.body;

    try {
        if (!nombre || !integrantes || !fecha_inicio || !pais) {
            res.status(400).json({"message": "Completar los campos obligatorios"})
        } else {
            next();
        }
    } catch (error) {
        res.status(400).json({"error": error.message})
    }
}

module.exports = { validateFormBand }