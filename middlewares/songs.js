const { getSongById } = require("../src/repositories/canciones");

exports.existsSong = async (req, res, next) => {
    const { song_id } = req.params;
    const song = await getSongById(song_id);
    console.log("existsSong:", song);
    if (!song){
        const jsonRes = {
            message:"No existe una cancion con esa id",
            error: true,
        }
        // jsonRes.cause = jsonRes.message;
        console.log(jsonRes);
        res.status(404).json(jsonRes);
        return
    }
    next();
}

exports.validateSongBody = (req, res, next) => {
    const { nombre, duracion, album, banda, fecha_publicacion } = req.body;

    if (typeof nombre !== "string" ||
        typeof duracion !== "number" ||
        typeof album !== "number" ||
        typeof banda !== "number" ||
        typeof fecha_publicacion !== "string"
    ) {
        res.status(400).json({message:"Los campos son invalidos", error: true});
        return 
    }

    next();
}