const express = require('express');
const route = express.Router();

const { existsSong, validateSongBody } = require('../middlewares/songs');
const { getSongs, getSongById, addSong, deleteSongById, modifySongById } = require('../src/repositories/canciones');


route.get('/', async (req, res) => {
    const songs = await getSongs();
    // if(!canciones?.length) return res.status(404).json({message:"No se han encontrado canciones"});
    res.status(200).json({songs});
});

route.get('/:song_id', async (req, res) => {
    const { song_id } = req.params;
    const canciones = await getSongById(song_id);
    
    if(!canciones?.length) return res.status(404).json({message:"No se ha encontrado una cancion con esa id"})
    res.status(200).json(canciones);
});

route.get('/:song_id', existsSong, async (req, res) => {
    const { song_id } = req.params;
    const song = await getSongById(song_id);
    res.status(200).json(song);
});

route.post('/', validateSongBody, async (req, res) => {
    try {
        addSong(req.body);
        res.status(201).json({message: "Cancion añadida", error: false});
    } catch (err) {
        console.log(`${err.message}\n${err.stack}`);
        res.status(500).json({message: "Error al añadir la canción", error: true, cause: err});
    }
});

route.put('/:song_id', validateSongBody, existsSong, async (req,res) => {
    const { song_id } = req.params;
    try {
        const result = await modifySongById(song_id, req.body)
        console.log(result);
        res.status(201).json({message: "Cancion modificada correctamente", error: false});
    } catch (err) {
        console.log(`${err.message}\nStack:\n${err.stack}`);
        res.status(500).json({message: "Error al intentar eliminar la canción", error: true, cause: err.message});
    }
});

route.delete('/:song_id',  existsSong, async (req, res) => {
    const { song_id } = req.params;
    try {
        const result = await deleteSongById(song_id);
        console.log(result);
        res.status(201).json({message: "Cancion eliminada correctamente", error: false, result});
    } catch (err) {
        console.log(`${err.message}\nStack:\n${err.stack}`);
        res.status(500).json({message: "Error al intentar eliminar la canción", error: true, cause: err.message});
    }
});


module.exports = route;