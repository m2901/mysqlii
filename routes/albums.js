const express = require('express');
const { validateData, validateIdParams, validateIdBand } = require('../middlewares/albums');
const router = express.Router();

const { getAlbums, getAlbum, postAlbums, putAlbum, deleteAlbum} = require('../src/repositories/controllers');



router.get('/', async (req, res) => {
    const albums = await getAlbums(); 
    res.json({ albums })
});

router.get('/:idAlbum', validateIdParams, async (req, res) => {
    const idAlbum = req.params.idAlbum;                
    const album = await getAlbum(idAlbum);
    res.json({ album })
});

router.post('/', validateData, validateIdBand, async (req, res) => {
    const { nombre, banda, fecha_publicacion } = req.body;
    const albums = await postAlbums(nombre, banda, fecha_publicacion);
    res.json({"msg": "Album Agregado"})
});

router.put('/:idAlbum', validateIdParams, validateData, validateIdBand, async (req, res) => {
    const idAlbum = req.params.idAlbum;
    const {nombre, banda, fecha_publicacion} = req.body
    const album = await putAlbum(nombre, banda, fecha_publicacion, idAlbum)
    res.status(200).json({"msg": "Album modificado"})
})

router.delete('/:idAlbum' ,validateIdParams, async (req, res) => {
    const idAlbum = req.params.idAlbum;
    const album = await deleteAlbum(idAlbum)
    res.status(200).json({"msg": "Album eliminado"}) 
})

module.exports = router