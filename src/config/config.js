require('dotenv').config({ });

const config = {
    node_port: process.env.NODE_PORT || 3000,
    db_port: process.env.DB_PORT || 3306,
    server: process.env.SERVER || 'mariadb://',
    user: process.env.USER || 'root',
    pass: process.env.PASS || '',
    host: process.env.HOST || 'localhost',
    db: process.env.DB || 'dwbe'
}

module.exports = config