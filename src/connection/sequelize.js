const config = require('../config/config');
const Sequelize = require('sequelize');

const sequelize = new Sequelize(`${config.server}${config.user}:${config.pass}@${config.host}:${config.db_port}/${config.db}`);

module.exports = sequelize;