const  sequelize  = require("../connection/sequelize")



exports.getBandas = async () => {
    const bandas = await sequelize.query('SELECT * FROM bands', { type: sequelize.QueryTypes.SELECT })
    return bandas
}


exports.postBandas = async () => {
    const bandas = await sequelize.query('SELECT * FROM bandasmusicales ', { type: sequelize.QueryTypes.INSERT })
    return bandas
}

exports.getAlbums = async () => {
    const albums = await sequelize.query('SELECT * FROM albums ', { type: sequelize.QueryTypes.SELECT })
    return albums
}

exports.getAlbum = async (idAlbum) => {
    const album = await sequelize.query(`SELECT * FROM albumes WHERE id = "${idAlbum}"`, { type: sequelize.QueryTypes.SELECT })
    return album
}

exports.postAlbums = async (nombre, banda, fecha_publicacion) => {
    let albums
    try {
        albums = await sequelize.query(`INSERT INTO albumes (nombre, banda, fecha_publicacion) VALUES ("${nombre}", "${banda}", "${fecha_publicacion}")`, { type: sequelize.QueryTypes.INSERT })

    } catch (error) {
        console.log(error)
    }
    return albums
}

exports.putAlbum = async (nombre, banda, fecha_publicacion, idAlbum) => {
    let album 
    try {
        album = await sequelize.query(`UPDATE albumes SET nombre = "${nombre}", banda = "${banda}", fecha_publicacion = "${fecha_publicacion}" WHERE id = "${idAlbum}"`, { type: sequelize.QueryTypes.UPDATE })
    } catch (error) {
        console.log(error)
    }
    return album;
}

exports.deleteAlbum = async (idAlbum) => {
    let album
    try {
        album = await sequelize.query(`DELETE FROM albumes WHERE id = "${idAlbum}"`, { type: sequelize.QueryTypes.DELETE })
    } catch (error) {
        console.log(error)
    }
}