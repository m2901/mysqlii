const sequelize  = require("../connection/sequelize")

exports.getSongs = async () => {
    const songs = await sequelize.query('SELECT * FROM songs', { type: sequelize.QueryTypes.SELECT});
    return songs;
}

exports.addSong = async (data) => {
    const { nombre, duracion, album, banda, fecha_publicacion } = data;
    console.log("añadiendo cancion:", data);
    await sequelize.query([
        'INSERT INTO canciones(nombre, duracion, album, banda, fecha_publicacion)',
        `VALUES ('${nombre}', ${duracion}, ${album}, ${banda}, '${fecha_publicacion}')`,
        ].join(" "),
        { type: sequelize.QueryTypes.INSERT }
    );
}

exports.getSongById = async (id) => {
    const song = await sequelize.query(
        `SELECT * FROM canciones WHERE id = ${id}`,
        { type: sequelize.QueryTypes.SELECT});
    return song[0];
}

exports.modifySongById = async (id, data) => {
    const { nombre, duracion, album, banda, fecha_publicacion } = data;
    const song = await sequelize.query([
            `UPDATE canciones`,
            `SET nombre='${nombre}', duracion=${duracion}, album=${album}, banda=${banda}, fecha_publicacion='${fecha_publicacion}'`,
            `WHERE id = ${id}`,
        ].join(" "),
        { type: sequelize.QueryTypes.DELETE }
    );
    return song;
}

exports.deleteSongById = async (id) => {
    const song = await sequelize.query(
        `DELETE FROM canciones WHERE id = ${id} RETURNING *`,
        { type: sequelize.QueryTypes.DELETE, }
    );
    return song;
}